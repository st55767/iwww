<!DOCTYPE HTML >
<html lang="cs">
<head>
    <title>Rezervační systém- rezervace</title>
    <link rel="stylesheet" type="text/css" href="ReservationWithStyle.css">

</head>
<body>
<?php
include('include/db_con.php');
session_start();
function deleteRecord($conPDO,$tableName, $idRecord){
    $stm=$conPDO->prepare("DELETE FROM $tableName WHERE id = ? ");
    $stm->bindParam(1,$idRecord);
    $stm->execute();
};

if(isset($_GET['id']) && isset($_GET['tableName'])) {
    $tableName = htmlspecialchars($_GET['tableName']);
    if ($tableName === "reservations" || $tableName === "destinations" || $tableName === "cars"
        || $tableName === "roles" || $tableName === "users") {
        $idRecord = htmlspecialchars($_GET['id']);
        deleteRecord($conPDO, $tableName, $idRecord);

    }
}
if (isset($_POST['back'])){
    header("Location:index.php");
}
if(isset($_POST['sub']))
{
    try {
    //$username=htmlspecialchars($_POST['name']);
   $description = htmlspecialchars($_POST['description']);
   if ($description==""){
       throw new Exception('Popis nesmí být prázdný');
   }
   $timeFrom = htmlspecialchars($_POST['timeFrom']);
   $timeTo = htmlspecialchars($_POST['timeTo']);
   $usersId= htmlspecialchars($_SESSION['idUzivatele']);
   $carsId = htmlspecialchars($_POST['carsId']);
   $destinationsId= htmlspecialchars($_POST['destinationsId']);
   $servisniProhlidka = htmlspecialchars($_POST['servis']);
   $eventStart= strtotime($timeFrom);
   $eventEnd = strtotime($timeTo);


    //if čas obsazen pro konkrétní auto
    $stm = $conPDO->prepare("SELECT * FROM reservations WHERE cars_idCars = ?");
    $stm->bindParam(1,$carsId);
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $reservationsResult= $stm->fetchAll();
        $kolize=0;
            foreach ($reservationsResult as $reservationsData):


            $oldEventstart =strtotime($reservationsData[2]) ;
            $oldEventend=strtotime($reservationsData[3]) ;

            if (($oldEventstart <= $eventStart) && ($oldEventend >= $eventStart) || ($oldEventstart <= $eventEnd && $oldEventend >= $eventEnd) ){
        $kolize = 1;

            }

endforeach;
            if ($eventStart == strtotime(0)|| $eventEnd ==strtotime(0) ){
                throw new Exception("Vyplňte čas prosím !");

            }

            if ($kolize == 0) {

                $s1 = "INSERT INTO reservations (description, timedateFrom, timedateTo, users_idUsers, cars_idCars, destinations_idDestinations, isServis) VALUES (?,?,?,?,?,?,?)";
                $stm = $conPDO->prepare($s1);
                $stm->bindParam(1, $description);
                $stm->bindParam(2, $timeFrom);
                $stm->bindParam(3, $timeTo);
                $stm->bindParam(4, $usersId);
                $stm->bindParam(5, $carsId);
                $stm->bindParam(6, $destinationsId);
                $stm->bindParam(7,$servisniProhlidka);
                $stm->execute();
            }else{
                throw new Exception('V tento čas auto není dostupné');

            }
}catch ( PDOException $ex){
        $error = "zadane parametry nejsou platne, v tento čas je jiný pracovník na cestě";
}
catch (Exception $e){
    $error= $e->getMessage();
}

}
?>


<div id="contenar">
    <header>    <h1 >Rezervační systém</h1>
       <div> <?php
        include "menu.php";
        ?></div>
    </header>

        <div id="rezervace" class="noPrint">
        <form action="reservation.php" method="POST">
<h2>Zadejte rezervaci</h2>
            <h3><?php
                if (isset($error)){
                    echo $error;
                }
                ?></h3>
            <table >

                <tr>
                    <td>Popis důvodu vypujčení</td>
                    <td >
                        <input name="description" type="text"  value="<?php if(isset($_POST['description'])){ echo $_POST['description']; }?>" /></td>
                </tr>
                <tr>
                    <td>Od: </td>
                    <td>
                        <input name="timeFrom" type="datetime-local" value="<?php if(isset($_POST['timeFrom'])){ echo $_POST['timeFrom']; }?>" /></td>

                </tr>

                <tr>
                    <td > Do: </td>
                    <td >
                        <input name="timeTo" type="datetime-local" value=" <?php if(isset($_POST['timeTo'])){ echo $_POST['timeTo']; }?> " /></td>
                </tr>


                    <?php

                    $carsQuery = "SELECT * FROM cars";
                    $stm= $conPDO->prepare($carsQuery);
                    $stm->execute();
                    $stm->setFetchMode(PDO::FETCH_NUM);
                    $carsResult = $stm->fetchAll();

                    $destinationsQuery = "SELECT * FROM destinations";
                    $stm=$conPDO->prepare($destinationsQuery);
                     $stm->execute();
                    $stm->setFetchMode(PDO::FETCH_NUM);
                    $destinationsResult =$stm->fetchAll();


?>
                <tr>
                    <td > Auto: </td>
                    <td>
                    <select name="carsId">
                        <?php
                       foreach($carsResult as $row2):;
                       if ($row2[3] == 1) {
                       ?>
                        <option value ="<?php echo $row2[0];?>"> <?php  echo $row2[1];?></option>
                        <?php }
                       endforeach; ?>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td > Cil trasy: </td>
                    <td>
                    <select name="destinationsId">
                        <?php
                        foreach ($destinationsResult as $row3):;
                        ?>
                        <option value="<?php echo $row3[0];?>"> <?php echo $row3[1];?></option>
                        <?php endforeach; ?>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>Servisni prohlidka :</td>
                    <td>
                        <select name="servis">
                            <option value="ano">ano</option>
                            <option value="ne">ne</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td >
                        <input type="submit" name="sub" value="Zarezervovat" /></td>
                <td><input type="submit" name="back" value="Odhlásit"/></td>
                </tr>

            </table>
        </form>
        </div>



</div>

</body>

</html>
