
<?php
include('include/db_con.php');
require ('paginace.php');

if (isset($_POST['json'])){
    $fileName = 'rezervace'. '.json';
    $file = fopen($fileName, 'w');
    $stm = $conPDO->prepare("SELECT * FROM reservations");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_OBJ);
    $reservationsResult= $stm->fetchAll();

    $text =  json_encode($reservationsResult);


    fwrite($file, $text);
    fclose($file);


    header('Content-type: application/json');
    header("Content-disposition: attachment; filename=\"" . $fileName);

   readfile($fileName);
    exit();

}



?>
<!DOCTYPE HTML >
<html lang="cs">
<head>
    <title>Rezervační systém- rezervace</title>
    <link rel="stylesheet" type="text/css" href="ReservationWithStyle.css">
    <link rel="stylesheet" href="./stranky.css" type="text/css"/>



</head>
<body>
<?php
session_start();
function deleteRecord($conPDO,$tableName, $idRecord){
    $stm=$conPDO->prepare("DELETE FROM $tableName WHERE id = ? ");
    $stm->bindParam(1,$idRecord);
    $stm->execute();
};

if(isset($_GET['id']) && isset($_GET['tableName'])) {
    $tableName = htmlspecialchars($_GET['tableName']);
    if ($tableName === "reservations" || $tableName === "destinations" || $tableName === "cars"
        || $tableName === "roles" || $tableName === "users") {
        $idRecord = htmlspecialchars($_GET['id']);
        deleteRecord($conPDO, $tableName, $idRecord);

    }
}
if (isset($_POST['back'])){
    header("Location:index.php");
}
if(isset($_POST['sub']))
{



    $username=htmlspecialchars($_POST['name']);
    $description = htmlspecialchars($_POST['description']);
    $timeFrom = htmlspecialchars($_POST['timeFrom']);
    $timeTo = htmlspecialchars($_POST['timeTo']);
    $usersId= htmlspecialchars($_POST['usersId']);
    $carsId = htmlspecialchars($_POST['carsId']);
    $destinationsId= htmlspecialchars($_POST['destinationsId']);
    try {


        $s1 = "INSERT INTO reservations (description, timedateFrom, timedateTo, users_idUsers, cars_idCars, destinations_idDestinations) VALUES (?,?,?,?,?,?)";
        $stm = $conPDO->prepare($s1);
        $stm->bindParam(1, $description);
        $stm->bindParam(2, $timeFrom);
        $stm->bindParam(3, $timeTo);
        $stm->bindParam(4, $usersId);
        $stm->bindParam(5, $carsId);
        $stm->bindParam(6, $destinationsId);
        $stm->execute();
        //mysqli_query($con,$s1);
    }catch ( PDOException $ex){
        $error = "zadane parametry nejsou platne, v tento čas je jiný pracovník na cestě";
    }

}
?>

<?php
if (isset($_GET['json'])){
    $stm = $conPDO->prepare("SELECT * FROM reservations");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_OBJ);
    $reservationsResult= $stm->fetchAll();

    $myJson = json_encode($reservationsResult);
    $filename = "soubor.json";
    $file=fopen($filename,"w");
    fwrite($file, $myJson);
    fclose($file);
    header("Location: ./$filename");


    //header("Content-Disposition: attachment; filename=$filename");
    // header("Content-Type: application/json");




}

if (isset($_POST['import']) && isset($_POST['text']) && $_POST['text']){
    $json = $_POST['text'];
    $result = json_decode($json);
    foreach ($result as $record){
        $rec=array();

        $rec[0]= htmlspecialchars($record->description);
        $rec[1]= htmlspecialchars($record->timedateFrom);
        $rec[2]= htmlspecialchars($record->timedateTo);
        $rec[3]= htmlspecialchars($record->users_idUsers);
        $rec[4]= htmlspecialchars($record->cars_idCars);
        $rec[5]= htmlspecialchars($record->destinations_idDestinations);
        $stm = $conPDO->prepare("insert into reservations(description, timedateFrom, timedateTo, users_idUsers, cars_idCars, destinations_idDestinations) values (?, ?, ?, ?, ?, ?)");
        $stm->bindParam(1,$rec[0]);
        $stm->bindParam(2,$rec[1]);
        $stm->bindParam(3,$rec[2]);
        $stm->bindParam(4,$rec[3]);
        $stm->bindParam(5,$rec[4]);
        $stm->bindParam(6,$rec[5]);
        $stm->execute();


    }
}
function vratUzivatele($conPDO,$strana, $naStranu)
{
    $pocatekOd = ($strana-1)*$naStranu;
    $conPDO->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
    $stm= $conPDO->prepare("SELECT reservations.description, reservations.timedateFrom, reservations.timedateTo, 
users.name, cars.carName,destinations.destinationName, reservations.cars_idCars  FROM reservations 
JOIN users ON users.id= reservations.users_idUser 
JOIN cars ON cars.id = reservations.cars_idCars 
JOIN destinations ON destinations.id = reservations.destinations_idDestinations
 ORDER BY reservations.id DESC LIMIT ?,?");


    $stm->bindParam(1,$pocatekOd);
    $stm->bindParam(2,$naStranu);
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $usersResult= $stm->fetchAll();




    return $usersResult;
}
function vratPocetUzivatelu($conPDO)
{
    $stm= $conPDO->prepare("SELECT COUNT(*) FROM reservations");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $result = $stm->fetchAll();
    return $result[0][0];
}

if (isset($_GET['strana']))
    $strana = $_GET['strana'];
else
    $strana = 1;
$naStranu = 5;
$reservationsResult = vratUzivatele($conPDO,$strana, $naStranu);
$stran = ceil(vratPocetUzivatelu($conPDO) / $naStranu);
?>


<div id="contenar">
    <header>    <h1 >Rezervační systém</h1>
        <div> <?php
            include "menu.php";
            ?></div>
    </header>



<section>
    <div id="header">

        <h2>Všechny rezervace</h2>
        <table>
<tr>
    <th>Popis</th>
    <th>Od: </th>
    <th>Do: </th>
    <th>Uzivatel</th>
    <th>Auto</th>
    <th>Destinace</th>


</tr>
            <?php
            foreach ($reservationsResult as $reservationsData):
                ?>
                <tr>
                    <td><?php echo $reservationsData[0];?></td>
                    <td><?php echo $reservationsData[1];?></td>
                    <td><?php echo $reservationsData[2];?></td>
                    <td><?php echo $reservationsData[3];?></td>
                    <td><a href="./car.php?name=<?php echo $reservationsData[6];?>" ><?php echo $reservationsData[4]?></a></td>
                    <td><?php echo $reservationsData[5];?></td>
                </tr>
            <?php
            endforeach;
            ?>
        </table>
    </div>



</section>
    <div>
        <?= paginace($strana, $stran, '?strana={strana}') ?>
    </div>
    <div id="JsonImport" class="noPrint">
        <form method="post">
            <input id="ulozbtn" type="submit" name="json" value="Ulozit do souboru" />

        </form>


        <form action="upload.php" method="post" enctype="multipart/form-data">
            Select json file to upload:
            <input type="file" name="fileToUpload" id="fileToUpload">
            <input type="submit" value="Upload" name="submit">
        </form>
    </div>

</div>

</body>

</html>
