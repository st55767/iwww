<?php
include "./include/db_con.php";
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// Check if file already exists
if (file_exists($target_file)) {
    echo "Soubor již existuje";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Soubor je příliš velký";
    $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo " Soubor nebyl nahrán";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file " . basename($_FILES["fileToUpload"]["name"]) . " has been uploaded.";
    $string = file_get_contents($target_file);

        $result = json_decode($string);

        foreach ($result as $record):;
            $rec=array();

            $rec[0]= htmlspecialchars($record->description);
            $rec[1]= htmlspecialchars($record->timedateFrom);
            $rec[2]= htmlspecialchars($record->timedateTo);
            $rec[3]= htmlspecialchars($record->users_idUsers);
            $rec[4]= htmlspecialchars($record->cars_idCars);
            $rec[5]= htmlspecialchars($record->destinations_idDestinations);
            $eventStart= strtotime($rec[1]);
            $eventEnd = strtotime($rec[2]);

            $stm = $conPDO->prepare("SELECT * FROM reservations WHERE cars_idCars = ?");
            $stm->bindParam(1,$rec[4]);
            $stm->execute();
            $stm->setFetchMode(PDO::FETCH_NUM);
            $reservationsResult= $stm->fetchAll();
            $kolize=0;
            foreach ($reservationsResult as $reservationsData):


                $oldEventstart =strtotime($reservationsData[2]) ;
                $oldEventend=strtotime($reservationsData[3]) ;

                if (($oldEventstart <= $eventStart) && ($oldEventend >= $eventStart) || ($oldEventstart <= $eventEnd && $oldEventend >= $eventEnd) ){
                    $kolize = 1;

                }

            endforeach;
if ($kolize == 0) {
    $stm = $conPDO->prepare("insert into reservations(description, timedateFrom, timedateTo, users_idUsers, cars_idCars, destinations_idDestinations) values (?, ?, ?, ?, ?, ?)");
    $stm->bindParam(1, $rec[0]);
    $stm->bindParam(2, $rec[1]);
    $stm->bindParam(3, $rec[2]);
    $stm->bindParam(4, $rec[3]);
    $stm->bindParam(5, $rec[4]);
    $stm->bindParam(6, $rec[5]);
    $stm->execute();
}

        endforeach;


    } else {
        echo "Je nám to líto, ale nastal problém s nahráváním vašeho souboru   ";
    }
}

?>
