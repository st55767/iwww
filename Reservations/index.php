<!DOCTYPE HTML >
<html lang="cs">
<head>
    <title>Rezervační systém</title>
    <link rel="stylesheet" type="text/css" href="ReservationWithStyle.css">
    <link rel="stylesheet" href="./stranky.css" type="text/css"/>



</head>
<body>
<div id="contenair">
    <?php
    include('include/db_con.php');
    require ('paginace.php');
    session_start();


        if (isset($_POST['registration'])){
            header("location:Registration.php");
        }
    ?>
    <?php
    if (isset($_POST['email'],$_POST['password']))
    {
        $username=$_POST['email'];
        $password = $_POST['password'];

        if (empty($username) || empty($password))
        {
            $error = 'Zadejte jmeno a heslo ';
        }

        else {
            $login=("select * from users where email= ? and password = ?");
           $stm = $conPDO->prepare($login);
           $stm->bindParam(1,$username);
           $stm->bindParam(2,$password);
           $stm->execute();
           $stm->setFetchMode(PDO::FETCH_NUM);
           $userResult= $stm->fetchAll();
           if ($userResult){

               if (isset($_POST['admin'])) {


                   if (count($userResult) != 0) {
                       $stm = $conPDO->prepare("SELECT admin from users WHERE id = ? ");
                       $stm->bindParam(1, $userResult[0][0]);
                       $stm->execute();
                       $stm->setFetchMode(PDO::FETCH_NUM);
                       $idRole = $stm->fetchAll();
                       if ($idRole[0][0] == 1) {
                           $_SESSION['idUzivatele'] = $userResult[0][0];
                           header('Location:./Admin/usersTables.php');
                           exit();
                       } else {
                           $error = "nejste admin";
                       }


                   } else {
                       $error = 'Nesprávné jméno nebo heslo';
                   }
               }

               $idUzivatele= $userResult[0][0];
               $_SESSION['idUzivatele'] = $userResult[0][0];

               header('Location:Reservation.php');



           } else {
               $error='Nesprávné jméno nebo heslo ';
           }

        }
    }

    $stm = $conPDO->prepare("SELECT * FROM reservations");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $reservationsResult= $stm->fetchAll();

    ?>

<header>    <h1 >Rezervační systém</h1>
</header>

    <section>

<div id="header">
    <p>Vítejte v rezervačním systému</p>
    <p>Pro zadání rezervace se přihlašte, pokud nemáte účet, tak se zaregistrujte.</p>
</div>
    <div id="prihlaseni">
        <form action="index.php" method="POST">
            <h2  id="h"><u><i>Přihlášení</i></u></h2>
            <?php  if (isset($error)) {?>
                    <h3><?php echo $error; ?></h3>
                    <br /> <br />
                <?php } ?>
            <table  id="t">

                <tr>
                    <td >Email:</td>
                    <td >
                        <input name="email" type="text"   /></td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td>
                        <input name="password" type="password"  /></td>
                </tr>
                <tr>
                    <td colspan="2" >
                        <input type="submit" name="login" value="Přihlásit" />

                        <input type="submit" name="registration" value="Zaregistrovat" />
                        <input type="submit" name="admin" value="admin"/>
                    </td>

                </tr>


            </table>
        </form>


    </div>
    </section>

    <div id="rezervace">
        <h2>Dosavadní rezervace</h2>
        <table class="tables">
            <?php
            function vratUzivatele($conPDO,$strana, $naStranu)
            {
                $pocatekOd = ($strana-1)*$naStranu;
                $conPDO->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
                $stm= $conPDO->prepare("SELECT * FROM reservations ORDER BY id  DESC LIMIT ?,?");


                $stm->bindParam(1,$pocatekOd);
                $stm->bindParam(2,$naStranu);
                $stm->execute();
                $stm->setFetchMode(PDO::FETCH_NUM);
                $usersResult= $stm->fetchAll();




                return $usersResult;
            }
            function vratPocetUzivatelu($conPDO)
            {
                $stm= $conPDO->prepare("SELECT COUNT(*) FROM reservations");
                $stm->execute();
                $stm->setFetchMode(PDO::FETCH_NUM);
                $result = $stm->fetchAll();
                return $result[0][0];
            }

            if (isset($_GET['strana']))
                $strana = $_GET['strana'];
            else
                $strana = 1;

            $naStranu = 5;
            $reservationsResult = vratUzivatele($conPDO,$strana, $naStranu);
            $stran = ceil(vratPocetUzivatelu($conPDO) / $naStranu);
            ?>


            <?php
            foreach ($reservationsResult as $reservationsData):

                ?>
                <tr>
                    <td><?php echo $reservationsData[1];?></td>
                    <td><?php echo $reservationsData[2];?></td>
                    <td><?php echo $reservationsData[3];?></td>
                    <?php
                    if ($reservationsData[7] == "ano"){
                        ?>
                        <td><?php
                            echo "servisní úkon";
                            ?></td>
                    <?php
                    }
                    ?>


                </tr>
            <?php
            endforeach;
            ?>

        </table>
    </div>
    <?= paginace($strana, $stran, '?strana={strana}') ?>

</div>
</body>
</html>
