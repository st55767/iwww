<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rezervační systém-update</title>
    <link rel="stylesheet" type="text/css" href="../ReservationWithStyle.css">

</head>
<body>
<header>    <h1 >Rezervační systém</h1>

</header>
<?php
require('../include/db_con.php');
$idUzivatele= '';
$idRole = '';
if (isset($_GET['sub'])) {

    $idRole = $_GET['idRole'];

    if (isset($_GET['id']) && $_GET['id']) {
        //update
        $query = "UPDATE users_has_roles set roles_idRoles = ? where users_idUsers= ?";
        $stm = $conPDO->prepare($query);
        $stm->bindParam(1,$idRole);
        $stm->bindParam(2,$_GET['id']);
        $stm->execute();

        header("Location:uhrTables.php");


    }else{
        //insert
        //select max id
        $idUzivatele= $_GET['idUzivatele'];
        $query= "INSERT INTO users_has_roles (users_idUsers ,roles_idRoles) values (?,?)";
        $stm=$conPDO->prepare($query);
        $stm->bindParam(1,$idUzivatele);
        $stm->bindParam(2,$idRole);

        $stm->execute();
        header("Location:uhrTables.php");
    }
}
?>
<div>
    <?php
    if (isset($_GET['id']) && $_GET['id']){

        $stm= $conPDO->prepare("SELECT * FROM users_has_roles WHERE users_idUsers= ?");
        $stm->bindParam(1,$_GET['id']);

        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $result= $stm->fetchAll();

        $idUzivatele= $result[0][0];
        $idRole =$result[0][1];

    }
    ?>
    <form action="usrHroleUpdate.php" method="get">
        <table>

            <tr>
                <td>ID uzivatele: </td>
                <td><input name="idUzivatele" type="text" id="nazev" value="<?php if (isset($idUzivatele)){ echo $idUzivatele;}?>"/></td>
            </tr>
            <tr>
                <td>ID role </td>
                <td><input name="idRole" type="text" id="idRole" value="<?php if (isset($idRole)){ echo $idRole;} ?>" /></td>
            </tr>
            <tr>
                <td><input type="submit" name="sub" value="Potvrdit" /></td>
            </tr>
            <tr>
                <?php
                if (isset($_GET['id'])){
                    ?>
                    <td><input name="id" type="hidden" value=" <?php if(isset($_GET['id'])){ echo $_GET['id'];}?>" /></td>
                    <?php
                }
                ?>

            </tr>
        </table>
    </form>
</div>
</body>
</html>


