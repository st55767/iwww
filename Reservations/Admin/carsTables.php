<?php
require('../include/db_con.php');
require ('../paginace.php');

    function deleteRecord($conPDO, $tableName, $idRecord)
    {
try {
        $stm = $conPDO->prepare("DELETE FROM $tableName WHERE id = ? ");
        $stm->bindParam(1, $idRecord);
        $stm->execute();
} catch (PDOException $PDOException) {
    echo "Toto auto je zarezervováno, proto ho nelze odstranit !";
}

    };

if (isset($_GET['odhlasit'])){
    header("Location:../index.php");
}
if(isset($_GET['id']) && isset($_GET['tableName'])){
    $tableName=htmlspecialchars($_GET['tableName']);
    if($tableName === "reservations" || $tableName === "destinations" ||$tableName === "cars"
        ||$tableName === "roles" ||$tableName === "users"  ){
        $idRecord=htmlspecialchars($_GET['id']);
        deleteRecord($conPDO,$tableName,$idRecord);

    }
    if ($tableName === "users_has_roles"){
        $idRecord= htmlspecialchars($_GET['id']);
        $stm= $conPDO->prepare("DELETE FROM users_has_roles WHERE users_idUsers = ?");
        $stm->bindParam(1,$idRecord);
        $stm->execute();
    }
}
function vratUzivatele($conPDO,$strana, $naStranu)
{
    $pocatekOd = ($strana-1)*$naStranu;
    $conPDO->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
    $stm= $conPDO->prepare("SELECT * FROM cars ORDER BY id DESC LIMIT ?,?");


    $stm->bindParam(1,$pocatekOd);
    $stm->bindParam(2,$naStranu);
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $usersResult= $stm->fetchAll();




    return $usersResult;
}
function vratPocetUzivatelu($conPDO)
{
    $stm= $conPDO->prepare("SELECT COUNT(*) FROM cars");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $result = $stm->fetchAll();
    return $result[0][0];
}

if (isset($_GET['strana']))
    $strana = $_GET['strana'];
else
    $strana = 1;
$naStranu = 15;
$carsResult = vratUzivatele($conPDO,$strana, $naStranu);
$stran = ceil(vratPocetUzivatelu($conPDO) / $naStranu);
?>
<!DOCTYPE html>

<html lang="cs-cz">
<head>
    <meta charset="utf-8" />
    <title>Users</title>
    <link rel="stylesheet" href="../stranky.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../ReservationWithStyle.css">

</head>

<body>
<header>    <h1 >Rezervační systém</h1>
    <div class="headermenu"> <?php
        include "adminMenu.php";
        ?></div>
</header>

<div>
    <h2>Cars</h2>
    <table >
<tr>
    <th>id</th>
    <th>Nazev auta</th>
    <th>SPZ</th>
    <th>Dostupne</th>
    </tr>

        <?php
        foreach ($carsResult as $carsData):

            ?>
            <tr>
                <td><?php echo $carsData[0];?></td>
                <td><?php echo $carsData[1];?></td>
                <td><?php echo $carsData[2];?></td>
                <td><?php echo $carsData[3];?></td>


                <td><a href="updateCars.php?id=<?php echo $carsData[0];?>" >update</a></td>
                <td><a href="?id=<?php echo $carsData[0];?>&tableName=cars " >delete</a></td>

            </tr>
        <?php
        endforeach;
        ?>
        <tr>
            <td><a href="updateCars.php">insert</a></td>
        </tr>
    </table>
</div>

<?= paginace($strana, $stran, '?strana={strana}') ?>
</body>
</html>

