<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rezervační systém-update</title>
    <link rel="stylesheet" type="text/css" href="../ReservationWithStyle.css">

</head>
<body>
<header>    <h1 >Rezervační systém</h1>

</header>
<?php
require('../include/db_con.php');
$description = '';
$timedateFrom = '';
$timedateTo = '';
$idUser = '';
$idCar = '';
$idDestination = '';


if (isset($_GET['sub'])) {

    $description = $_GET['description'];
    $timedateFrom= $_GET['timeFrom'];
    $timedateTo= $_GET['timeTo'];
    try{
        if ($timedateTo == 0 || $timedateFrom == 0){
throw new Exception("Vyplntě datum a čas");
        }
    }  catch (Exception $e){
        $error= $e->getMessage();
    }


    $idCar= $_GET['idCar'];
    $idDestination= $_GET['idDestination'];
    $eventStart = strtotime($timedateFrom);
    $eventEnd = strtotime($timedateTo);

    if (isset($_GET['id']) && $_GET['id']) {
        //update
        try {
            $stm = $conPDO->prepare("SELECT * FROM reservations WHERE cars_idCars = ?");
            $stm->bindParam(1, $idCar);
            $stm->execute();
            $stm->setFetchMode(PDO::FETCH_NUM);
            $reservationsResult = $stm->fetchAll();
            $kolize = 0;
            foreach ($reservationsResult as $reservationsData):


                $oldEventstart = strtotime($reservationsData[2]);
                $oldEventend = strtotime($reservationsData[3]);

                if (($oldEventstart <= $eventStart) && ($oldEventend >= $eventStart) || ($oldEventstart <= $eventEnd && $oldEventend >= $eventEnd)) {
                    if ($reservationsData[0] == $_GET['id']){
                        $kolize = 0;
                    }else{
                        $kolize = 1;
                    }


                }

            endforeach;

            if ($kolize == 0) {
                $query = "UPDATE reservations set description= ?,timedateFrom=?, timedateTo=?,cars_idCars =?, destinations_idDestinations =? where id= ?";
                $stm = $conPDO->prepare($query);
                $stm->bindParam(1, $description);
                $stm->bindParam(2, $timedateFrom);
                $stm->bindParam(3, $timedateTo);

                $stm->bindParam(4, $idCar);
                $stm->bindParam(5, $idDestination);
                $stm->bindParam(6, $_GET['id']);
                $stm->execute();
            }else{
                throw new Exception('V tento čas auto není dostupné');

            }
        }catch (PDOException $ex){
            $error = "Tento záznam nelze vložit do databáze";
        }   catch (Exception $e){
            $error= $e->getMessage();
        }
if ($kolize == 0) {
    header("Location:reservationsTables.php");
}else {
    ?>
    <h3><?php
        if (isset($error)){
            echo $error;
        }
        ?></h3>
    <?php
}
    }else{
//insert

        try {

            //if čas obsazen pro konkrétní auto
            $stm = $conPDO->prepare("SELECT * FROM reservations WHERE cars_idCars = ?");
            $stm->bindParam(1,$idCar);
            $stm->execute();
            $stm->setFetchMode(PDO::FETCH_NUM);
            $reservationsResult= $stm->fetchAll();
            $kolize=0;
            foreach ($reservationsResult as $reservationsData):


                $oldEventstart =strtotime($reservationsData[2]) ;
                $oldEventend=strtotime($reservationsData[3]) ;

                if (($oldEventstart <= $eventStart) && ($oldEventend >= $eventStart) || ($oldEventstart <= $eventEnd && $oldEventend >= $eventEnd) ){
                    $kolize = 1;

                }

            endforeach;

            if ($kolize == 0) {

                $s1 = "INSERT INTO reservations (description, timedateFrom, timedateTo, users_idUsers, cars_idCars, destinations_idDestinations) VALUES (?,?,?,?,?,?)";
                $stm = $conPDO->prepare($s1);
                $stm->bindParam(1, $description);
                $stm->bindParam(2, $timedateFrom);
                $stm->bindParam(3, $timedateTo);
                $usersId =  $_GET['cislo'];
                $stm->bindParam(4, $usersId);
                $stm->bindParam(5, $carsId);
                $stm->bindParam(6, $destinationsId);
                $stm->execute();
                header("Location:reservationsTables.php");
            }else{
                throw new Exception('V tento čas auto není dostupné');

            }
        }catch ( PDOException $ex){
            $error = $ex->getMessage();
        }
        catch (Exception $e){
            $error= $e->getMessage();
        }
    }
}
?>
<div>
    <?php
    if (isset($error)){
        echo $error;
    }
    if (isset($_GET['id']) && $_GET['id']){

        $stm= $conPDO->prepare("SELECT * FROM reservations WHERE id= ?");
        $stm->bindParam(1,$_GET['id']);

        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $result= $stm->fetchAll();


        $description =$result[0][1];
        $timedateFrom= $result[0][2];
        $timedateTo= $result[0][3];
        $idUser= $result[0][4];
        $idCar= $result[0][5];
        $idDestination= $result[0][6];
    }
    ?>
    <form action="updateReservations.php" method="get">
        <?php
        $usersQuery = "SELECT * FROM users";
        $stm= $conPDO->prepare($usersQuery);
        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $usersResult = $stm->fetchAll();
        $carsQuery = "SELECT * FROM cars";
        $stm=$conPDO->prepare($carsQuery);
        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $carsResult = $stm->fetchAll();
        $destinationsQuery = "SELECT * FROM destinations";
        $stm= $conPDO->prepare($destinationsQuery);
        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $destinationsResult = $stm->fetchAll();

        ?>
        <table>

            <tr>
                <td>Popis: </td>
                <td><input name="description" type="text" id="desription" value="<?php if (isset($description)){ echo $description;}?>"/></td>
            </tr>
            <tr>
                <td>Od: </td>
                <td><input name="timeFrom" type="datetime-local" id="timeFrom" value="<?php if (isset($timedateFrom)){ echo $timedateFrom;} ?>" /></td>
            </tr>
            <tr>
                <td>Do: </td>
                <td><input name="timeTo" type="datetime-local" id="timeTo" value="<?php if (isset($timedateTo)){ echo $timedateTo;} ?>" /></td>
            </tr>

            <tr>
                <td > Auto: </td>
                <td>
                    <select name="idCar">
                        <?php
                        foreach ($carsResult as $res):
                            ?>
                            <option value="<?php echo $res[0]; ?>"><?php echo $res[1];?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td > Destinace: </td>
                <td>
                    <select name="idDestination">
                        <?php
                        foreach ($destinationsResult as $res):
                            ?>
                            <option  value="<?php echo $res[0]; ?>"><?php echo $res[1];?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td><input type="submit" name="sub" value="Potvrdit" /></td>
            </tr>
            <tr>
                <?php
                if (isset($_GET['id'])){
                    ?>
                    <td><input name="id" type="hidden" value=" <?php if(isset($_GET['id'])){ echo $_GET['id'];}?>" /></td>
                    <?php
                }
                ?>



            </tr>
        </table>
    </form>
</div>
</body>
</html>

