<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rezervační systém-update</title>
    <link rel="stylesheet" type="text/css" href="../ReservationWithStyle.css">

</head>
<body>
<header>    <h1 >Rezervační systém</h1>

</header>
<?php
require('../include/db_con.php');
$name = '';
$SPZ = '';
if (isset($_GET['sub'])) {

    $name = $_GET['nazev'];
    $SPZ = $_GET['SPZ'];
    $dostupne = $_GET['dostupne'];
    if ($name == "" || $SPZ == ""){
echo "zadejte název a SPZ auta";
    }else {
        try {
            $stm = $conPDO->prepare("SELECT carSPZ, id FROM cars ");
            $stm->execute();
            $stm->setFetchMode(PDO::FETCH_NUM);
            $allSPZ = $stm->fetchAll();
            foreach ($allSPZ as $carsSPZ):;
                if (isset($_GET['id']) ){
                    if ($_GET['id'] != $carsSPZ[1]){
                        if ($SPZ == $carsSPZ[0]) {
                            throw new Exception("Zadaná SPZ je již v databázi!");
                        }
                    }
                }else if ($SPZ == $carsSPZ[0]) {
                    throw new Exception("Zadaný SPZ je již v databázi !");
                }
            endforeach;
            if (isset($_GET['id']) && $_GET['id']) {
                //update
                $query = "UPDATE cars set carName = ?, carSPZ= ?, dostupne=? where id= ?";
                $stm = $conPDO->prepare($query);
                $stm->bindParam(1, $name);
                $stm->bindParam(2, $SPZ);
                $stm->bindParam(3, $dostupne);
                $stm->bindParam(4, $_GET['id']);
                $stm->execute();

                header("Location:carsTables.php");


            } else {
                //insert


                $query = "INSERT INTO cars (carName, carSPZ, dostupne) values (?,?,?)";
                $stm = $conPDO->prepare($query);
                $stm->bindParam(1, $name);
                $stm->bindParam(2, $SPZ);
                $stm->bindParam(3, $dostupne);

                $stm->execute();
                header("Location:carsTables.php");
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
        }
    }
}
?>
<div>
    <?php
    if (isset($_GET['id']) && $_GET['id']){

        $stm= $conPDO->prepare("SELECT * FROM cars WHERE id= ?");
        $stm->bindParam(1,$_GET['id']);
        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $result= $stm->fetchAll();


        $name =$result[0][1];
        $SPZ = $result[0][2];
        $dostupne = $result[0][3];

    }
    ?>
    <form action="updateCars.php" method="get">
        <?php
        if (isset($error)){
            echo $error;
        }
        ?>
        <table>

        <tr>
            <td>Název auta: </td>
            <td><input name="nazev" type="text" id="nazev" value="<?php if (isset($name)){ echo $name;}?>"/></td>
        </tr>
        <tr>
            <td>SPZ: </td>
            <td><input name="SPZ" type="text" id="SPZ" value="<?php if (isset($SPZ)){ echo $SPZ;} ?>" /></td>
        </tr>
        <tr>
            <td>dostupne: </td>
            <td><input name="dostupne" type="text" id="dostupne" value="<?php if (isset($dostupne)){ echo $dostupne;} ?>" /></td>
        </tr>
        <tr>
            <td><input type="submit" name="sub" value="Potvrdit" /></td>
        </tr>
        <tr>
            <?php
            if (isset($_GET['id'])){
               ?>
                <td><input name="id" type="hidden" value=" <?php if(isset($_GET['id'])){ echo $_GET['id'];}?>" /></td>
            <?php
            }
            ?>

        </tr>
    </table>
    </form>
</div>
</body>
</html>

