<?php
require('../include/db_con.php');
require ('../paginace.php');
function deleteRecord($conPDO,$tableName, $idRecord){
    $stm=$conPDO->prepare("DELETE FROM $tableName WHERE id = ? ");
    $stm->bindParam(1,$idRecord);
    $stm->execute();
};
if (isset($_GET['odhlasit'])){
    header("Location:../index.php");
}
if(isset($_GET['id']) && isset($_GET['tableName'])){
    $tableName=htmlspecialchars($_GET['tableName']);
    if($tableName === "reservations" || $tableName === "destinations" ||$tableName === "cars"
        ||$tableName === "roles" ||$tableName === "users"  ){
        $idRecord=htmlspecialchars($_GET['id']);
        deleteRecord($conPDO,$tableName,$idRecord);

    }
    if ($tableName === "users_has_roles"){
        $idRecord= htmlspecialchars($_GET['id']);
        $stm= $conPDO->prepare("DELETE FROM users_has_roles WHERE users_idUsers = ?");
        $stm->bindParam(1,$idRecord);
        $stm->execute();
    }
}
function vratUzivatele($conPDO,$strana, $naStranu)
{
    $pocatekOd = ($strana-1)*$naStranu;
    $conPDO->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
    $stm= $conPDO->prepare("SELECT * FROM users ORDER BY id DESC LIMIT ?,?");


    $stm->bindParam(1,$pocatekOd);
    $stm->bindParam(2,$naStranu);
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $usersResult= $stm->fetchAll();




    return $usersResult;
}
function vratPocetUzivatelu($conPDO)
{
    $stm= $conPDO->prepare("SELECT COUNT(*) FROM users");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $result = $stm->fetchAll();
    return $result[0][0];
}

if (isset($_GET['strana']))
    $strana = $_GET['strana'];
else
    $strana = 1;
$naStranu = 15;
$usersResult = vratUzivatele($conPDO,$strana, $naStranu);
$stran = ceil(vratPocetUzivatelu($conPDO) / $naStranu);
?>
<!DOCTYPE html>

<html lang="cs-cz">
<head>
    <meta charset="utf-8" />
    <title>Users</title>
    <link rel="stylesheet" href="../stranky.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../ReservationWithStyle.css">

</head>

<body>
<header>    <h1 >Rezervační systém</h1>
    <div class="headermenu"> <?php
        include "adminMenu.php";
        ?></div>
</header>

<table class="tabulka">
    <tr>
        <th>Id</th>
        <th>Jmeno</th>
        <th>Příjmení</th>
        <th>Email</th>
        <th>Heslo</th>
    </tr>
    <?php
    foreach ($usersResult as $usersData):

        ?>


        <tr>
            <td><?php echo $usersData[0];?></td>
            <td><?php echo $usersData[1];?></td>
            <td><?php echo $usersData[2];?></td>
            <td><?php echo $usersData[3];?></td>
            <td><?php echo $usersData[4];?></td>

            <td><a href="updateUsers.php?id=<?php echo $usersData[0];?>" >update</a></td>
            <td><a href="?id=<?php echo $usersData[0];?>&tableName=users">delete</a></td>

        </tr>
    <?php
    endforeach;
    ?>
    <tr>
        <td><a href="updateUsers.php">insert</a></td>
    </tr>
</table>



<?= paginace($strana, $stran, '?strana={strana}') ?>
</body>
</html>
