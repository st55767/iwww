<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rezervační systém-update</title>
    <link rel="stylesheet" type="text/css" href="../ReservationWithStyle.css">

</head>
<body>
<header>    <h1 >Rezervační systém</h1>

</header>
<?php
require('../include/db_con.php');

$name = '';
$description= '';
$numberOfKilometers = 0 ;
if (isset($_GET['sub'])) {

    $name = $_GET['nazev'];
    $description = $_GET['popis'];
    $numberOfKilometers = $_GET['kilometry'];
    if ($name == "" || $numberOfKilometers == 0) {
echo "zadejte alespoň název destinace a vzdálenost";
    } else {
        if (isset($_GET['id']) && $_GET['id']) {
            //update
            $query = "UPDATE destinations set destinationName = ?, description= ? , numberOfKilometers = ? where id= ?";
            $stm = $conPDO->prepare($query);
            $stm->bindParam(1, $name);
            $stm->bindParam(2, $description);
            $stm->bindParam(3,$numberOfKilometers);
            $stm->bindParam(4, $_GET['id']);
            $stm->execute();

            header("Location:destinationsTables.php");


        } else {
            //insert

            $query = "INSERT INTO destinations (destinationName, description, numberOfKilometers) values (?,?,?)";
            $stm = $conPDO->prepare($query);

            $stm->bindParam(1, $name);
            $stm->bindParam(2, $description);
            $stm->bindParam(3,$numberOfKilometers);
            $stm->execute();
            header("Location:destinationsTables.php");
        }
    }
}

?>
<div>
    <?php
    if (isset($_GET['id']) && $_GET['id']){

        $stm= $conPDO->prepare("SELECT * FROM destinations WHERE id= ?");
        $stm->bindParam(1,$_GET['id']);

        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $result= $stm->fetchAll();


        $name =$result[0][1];
        $description = $result[0][2];

    }
    ?>
    <form action="updateDestinations.php" method="get">
        <table>

            <tr>
                <td>Název destinace: </td>
                <td><input name="nazev" type="text" id="nazev" value="<?php if (isset($name)){ echo $name;}?>"/></td>
            </tr>
            <tr>
                <td>Popis: </td>
                <td><input name="popis" type="text" id="popis" value="<?php if (isset($description)){ echo $description;} ?>" /></td>
            </tr>
            <tr>
                <td>Popis: </td>
                <td><input name="kilometry" type="number" id="kilometry" value="<?php if (isset($numberOfKilometers)){ echo $numberOfKilometers;} ?>" /></td>
            </tr>
            <tr>
                <td><input type="submit" name="sub" value="Potvrdit" /></td>
            </tr>
            <tr>
                <?php
                if (isset($_GET['id'])){
                    ?>
                    <td><input name="id" type="hidden" value=" <?php if(isset($_GET['id'])){ echo $_GET['id'];}?>" /></td>
                    <?php
                }
                ?>

            </tr>
        </table>
    </form>
</div>
</body>
</html>

