<?php
require('../include/db_con.php');
require ('../paginace.php');
function deleteRecord($conPDO,$tableName, $idRecord){
    $stm=$conPDO->prepare("DELETE FROM $tableName WHERE id = ? ");
    $stm->bindParam(1,$idRecord);
    $stm->execute();
};
if (isset($_GET['odhlasit'])){
    header("Location:../index.php");
}
if(isset($_GET['id']) && isset($_GET['tableName'])){
    $tableName=htmlspecialchars($_GET['tableName']);
    if($tableName === "reservations" || $tableName === "destinations" ||$tableName === "cars"
        ||$tableName === "roles" ||$tableName === "users"  ){
        $idRecord=htmlspecialchars($_GET['id']);
        deleteRecord($conPDO,$tableName,$idRecord);

    }
    if ($tableName === "users_has_roles"){
        $idRecord= htmlspecialchars($_GET['id']);
        $stm= $conPDO->prepare("DELETE FROM users_has_roles WHERE users_idUsers = ?");
        $stm->bindParam(1,$idRecord);
        $stm->execute();
    }
}
function vratUzivatele($conPDO,$strana, $naStranu)
{
    $pocatekOd = ($strana-1)*$naStranu;
    $conPDO->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
    $stm= $conPDO->prepare("SELECT * FROM reservations ORDER BY id  DESC LIMIT ?,?");


    $stm->bindParam(1,$pocatekOd);
    $stm->bindParam(2,$naStranu);
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $usersResult= $stm->fetchAll();




    return $usersResult;
}
function vratPocetUzivatelu($conPDO)
{
    $stm= $conPDO->prepare("SELECT COUNT(*) FROM reservations");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $result = $stm->fetchAll();
    return $result[0][0];
}

if (isset($_GET['strana']))
    $strana = $_GET['strana'];
else
    $strana = 1;
$naStranu =10;
$reservationsResult = vratUzivatele($conPDO,$strana, $naStranu);
$stran = ceil(vratPocetUzivatelu($conPDO) / $naStranu);
?>
<!DOCTYPE html>

<html lang="cs-cz">
<head>
    <meta charset="utf-8" />
    <title>Users</title>
    <link rel="stylesheet" href="../stranky.css" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="../ReservationWithStyle.css">

</head>

<body>
<header>    <h1 >Rezervační systém</h1>
    <div class="headermenu"> <?php
        include "adminMenu.php";
        ?></div>
</header>

<div>
    <h2>Reservations</h2>
    <table >

        <tr>
            <th>id</th>
            <th>Popis</th>
            <th>Čas od:</th>
            <th>Čas do:</th>
            <th>id uzivatele</th>
            <th>id auta</th>
            <th>id destinace</th>
        </tr>
        <?php
        foreach ($reservationsResult as $reservationsData):

            ?>
            <tr>
                <td><?php echo $reservationsData[0];?></td>
                <td><?php echo $reservationsData[1];?></td>
                <td><?php echo $reservationsData[2];?></td>
                <td><?php echo $reservationsData[3];?></td>
                <td><?php echo $reservationsData[4];?></td>
                <td><?php echo $reservationsData[5];?></td>
                <td><?php echo $reservationsData[6];?></td>

                <td><a href="updateReservations.php?id=<?php echo $reservationsData[0];?>&cislo=<?php echo $reservationsData[4]?>" >update</a></td>
                <td><a href="?id=<?php echo $reservationsData[0];?>&tableName=reservations " >delete</a></td>

            </tr>
        <?php
        endforeach;
        ?>
        <tr>
            <td><a href="updateReservations.php">insert</a></td>
        </tr>
    </table>
</div>


<?= paginace($strana, $stran, '?strana={strana}') ?>
</body>
</html>
