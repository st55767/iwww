<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rezervační systém-update</title>
    <link rel="stylesheet" type="text/css" href="../ReservationWithStyle.css">

</head>
<body>
<header>    <h1 >Rezervační systém</h1>

</header>
<?php
require('../include/db_con.php');
$name = '';
$surname = '';
$email = '';
$password = '';

if (isset($_GET['sub'])) {
try {
    $name = $_GET['nazev'];
    $surname = $_GET['surname'];
    $email = $_GET['email'];
    $password = $_GET['password'];
    //select email
    $stm = $conPDO->prepare("SELECT email, id FROM users ");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $allemails = $stm->fetchAll();
    foreach ($allemails as $usersEmail):;

            if (isset($_GET['id']) ){
                if ($_GET['id'] != $usersEmail[1]){
                    if ($email == $usersEmail[0]) {
                        throw new Exception("Zadaný email je již používán !");
                }
            }
        }else if ($email == $usersEmail[0]) {
                throw new Exception("Zadaný email je již používán !");
            }

    endforeach;

    if ($name == "" || $surname == "" || $email == "" || $password == "") {

    } else {
        if (isset($_GET['id']) && $_GET['id']) {
            //update
            $query = "UPDATE users set name = ?, surname= ?, email = ?, password = ? where id= ?";
            $stm = $conPDO->prepare($query);
            $stm->bindParam(1, $name);
            $stm->bindParam(2, $surname);
            $stm->bindParam(3, $email);
            $stm->bindParam(4, $password);
            $stm->bindParam(5, $_GET['id']);
            $stm->execute();

            header("Location:usersTables.php");


        } else {
            //insert

            $query = "INSERT INTO users (name, surname, email, password) values (?,?,?,?)";
            $stm = $conPDO->prepare($query);

            $stm->bindParam(1, $name);
            $stm->bindParam(2, $surname);
            $stm->bindParam(3, $email);
            $stm->bindParam(4, $password);

            $stm->execute();
            header("Location:usersTables.php");
        }
    }
}catch (Exception $e){
$error= $e->getMessage();
}
}
?>
<div>
    <?php
    if (isset($_GET['id']) && $_GET['id']){

        $stm= $conPDO->prepare("SELECT * FROM users WHERE id= ?");
        $stm->bindParam(1,$_GET['id']);

        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $result= $stm->fetchAll();


        $name =$result[0][1];
        $surname = $result[0][2];
        $email = $result[0][3];
        $password = $result[0][4];
    }
    ?>
    <form action="updateUsers.php" method="get">
        <?php
        if (isset($error))
            echo $error
        ?>
        <table>

            <tr>
                <td>Jméno: </td>
                <td><input name="nazev" type="text" id="nazev" value="<?php if (isset($name)){ echo $name;}?>"/></td>
            </tr>
            <tr>
                <td>Příjmění: </td>
                <td><input name="surname" type="text" id="surname" value="<?php if (isset($surname)){ echo $surname;} ?>" /></td>
            </tr>
            <tr>
                <td>email: </td>
                <td><input name="email" type="text" id="email" value="<?php if (isset($email)){ echo $email;} ?>" /></td>
            </tr>
            <tr>
                <td>heslo: </td>
                <td><input name="password" type="password" id="password" value="<?php if (isset($password)){ echo $password;} ?>" /></td>
            </tr>
            <tr>
                <td><input type="submit" name="sub" value="Potvrdit" /></td>
            </tr>
            <tr>
                <?php
                if (isset($_GET['id'])){
                    ?>
                    <td><input name="id" type="hidden" value=" <?php if(isset($_GET['id'])){ echo $_GET['id'];}?>" /></td>
                    <?php
                }
                ?>

            </tr>
        </table>
    </form>
</div>
</body>
</html>

