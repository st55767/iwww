<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rezervační systém-update</title>
    <link rel="stylesheet" type="text/css" href="../ReservationWithStyle.css">

</head>
<body>
<header>    <h1 >Rezervační systém</h1>

</header>
<?php
require('../include/db_con.php');

$name = '';
if (isset($_GET['sub'])) {



    $name = $_GET['nazev'];

    if ($name == "") {
echo "Zadejte název role";
    } else {
        if (isset($_GET['id']) && $_GET['id']) {
            //update
            $query = "UPDATE roles set roleName = ? where id= ?";
            $stm = $conPDO->prepare($query);
            $stm->bindParam(1, $name);
            $stm->bindParam(2, $_GET['id']);
            $stm->execute();

            header("Location:rolesTables.php");


        } else {
            //insert


            $query = "INSERT INTO roles (roleName) values (?)";
            $stm = $conPDO->prepare($query);

            $stm->bindParam(1, $name);

            $stm->execute();
            header("Location:rolesTables.php");
        }
    }
}
?>
<div>
    <?php
    if (isset($_GET['id']) && $_GET['id']){

        $stm= $conPDO->prepare("SELECT * FROM roles WHERE id= ?");
        $stm->bindParam(1,$_GET['id']);

        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $result= $stm->fetchAll();


        $name =$result[0][1];


    }
    ?>
    <form action="updateRoles.php" method="get">
        <table>

            <tr>
                <td>Název role: </td>
                <td><input name="nazev" type="text" id="nazev" value="<?php if (isset($name)){ echo $name;}?>"/></td>
            </tr>

            <tr>
                <td><input type="submit" name="sub" value="Potvrdit" /></td>
            </tr>
            <tr>
                <?php
                if (isset($_GET['id'])){
                    ?>
                    <td><input name="id" type="hidden" value=" <?php if(isset($_GET['id'])){ echo $_GET['id'];}?>" /></td>
                    <?php
                }
                ?>

            </tr>
        </table>
    </form>
</div>
</body>
</html>

