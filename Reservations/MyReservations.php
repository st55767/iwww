<!DOCTYPE HTML >
<html lang="cs">
<head>
    <title>Rezervační systém- rezervace</title>
    <link rel="stylesheet" type="text/css" href="ReservationWithStyle.css">
    <link rel="stylesheet" href="./stranky.css" type="text/css"/>


</head>
<body>
<?php
include('include/db_con.php');
require ('paginace.php');

session_start();
function deleteRecord($conPDO,$tableName, $idRecord){
    $stm=$conPDO->prepare("DELETE FROM $tableName WHERE id = ? ");
    $stm->bindParam(1,$idRecord);
    $stm->execute();
};

if(isset($_GET['id']) && isset($_GET['tableName'])) {
    $tableName = htmlspecialchars($_GET['tableName']);
    if ($tableName === "reservations" || $tableName === "destinations" || $tableName === "cars"
        || $tableName === "roles" || $tableName === "users") {
        $idRecord = htmlspecialchars($_GET['id']);
        deleteRecord($conPDO, $tableName, $idRecord);

    }
}
if (isset($_POST['back'])){
    header("Location:index.php");
}
function vratUzivatele($conPDO,$strana, $naStranu, $idUzivatele)
{
    $pocatekOd = ($strana-1)*$naStranu;
    $conPDO->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
    $stm= $conPDO->prepare("SELECT * FROM reservations where users_idUsers = ? ORDER BY id  DESC LIMIT ?,?");

$stm->bindParam(1,$idUzivatele);
    $stm->bindParam(2,$pocatekOd);
    $stm->bindParam(3,$naStranu);
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $usersResult= $stm->fetchAll();




    return $usersResult;
}
function vratPocetUzivatelu($conPDO)
{
    $stm= $conPDO->prepare("SELECT COUNT(*) FROM reservations");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $result = $stm->fetchAll();
    return $result[0][0];
}

if (isset($_GET['strana']))
    $strana = $_GET['strana'];
else
    $strana = 1;

$naStranu = 10;
$idUzivatele = $_SESSION['idUzivatele'];
$reservationsResult = vratUzivatele($conPDO,$strana, $naStranu, $idUzivatele);
$stran = ceil(vratPocetUzivatelu($conPDO) / $naStranu);
?>




<div id="contenar">
    <header>    <h1 >Rezervační systém</h1>
        <div> <?php
            include "menu.php";
            ?></div>
    </header>


<section>
    <div id="header">

        <h2>Vaše rezervace</h2>
        <table>
            <tr>
                <th>Popis</th>
                <th>Od: </th>
                <th>Do: </th>
                <th>Uzivatel</th>
                <th>Auto</th>
                <th>Destinace</th>


            </tr>

            <?php
            foreach ($reservationsResult as $reservationsData):

                ?>
                <tr>
                    <td><?php echo $reservationsData[1];?></td>
                    <td><?php echo $reservationsData[2];?></td>
                    <td><?php echo $reservationsData[3];?></td>
                    <td><?php echo $reservationsData[4];?></td>
                    <td><?php echo $reservationsData[5];?></td>
                    <td><?php echo $reservationsData[6];?></td>

                    <td><a href="./updateUserReservations.php?id=<?php echo $reservationsData[0];?>" >update</a></td>
                    <td><a href="?id=<?php echo $reservationsData[0];?>&tableName=reservations " >delete</a></td>

                </tr>
            <?php
            endforeach;
            ?>

        </table>
    </div>
    <div id="prihlaseni">
        <?php
        $usersQuery = "SELECT * FROM users";
        $stm = $conPDO->prepare($usersQuery);
        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $usersResult=$stm->fetchAll();

        $carsQuery = "SELECT * FROM cars";
        $stm= $conPDO->prepare($carsQuery);
        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $carsResult = $stm->fetchAll();

        $destinationsQuery = "SELECT * FROM destinations";
        $stm=$conPDO->prepare($destinationsQuery);
        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $destinationsResult =$stm->fetchAll();
        ?>
        <ul>
            <li><?php
                foreach ($usersResult as $row1):;
                    echo $row1[0];
                    echo "-";
                    echo $row1[1];
                    echo "  ";
                endforeach;
                ?></li>
            <li>
                <?php
                foreach ($carsResult as $row2):;
                    echo $row2[0];
                    echo "-";
                    echo $row2[1];
                    echo "  ";
                endforeach;
                ?>
            </li>
            <li>
                <?php
                foreach ($destinationsResult as $row3):;
                    echo $row3[0];
                    echo "-";
                    echo $row3[1];
                    echo "  ";
                endforeach;
                ?>
            </li>
        </ul>
    </div>
</section>




</div>
<?= paginace($strana, $stran, '?strana={strana}') ?>
</body>

</html>
