<!DOCTYPE HTML >
<html lang="cs">
<head>
    <title>Rezervační systém- rezervace</title>
    <link rel="stylesheet" type="text/css" href="ReservationWithStyle.css">
    <link rel="stylesheet" href="./stranky.css" type="text/css"/>


</head>
<body>
<?php
include('include/db_con.php');
require ('paginace.php');

session_start();
function deleteRecord($conPDO,$tableName, $idRecord){
    $stm=$conPDO->prepare("DELETE FROM $tableName WHERE id = ? ");
    $stm->bindParam(1,$idRecord);
    $stm->execute();
};

if(isset($_GET['id']) && isset($_GET['tableName'])) {
    $tableName = htmlspecialchars($_GET['tableName']);
    if ($tableName === "reservations" || $tableName === "destinations" || $tableName === "cars"
        || $tableName === "roles" || $tableName === "users") {
        $idRecord = htmlspecialchars($_GET['id']);
        deleteRecord($conPDO, $tableName, $idRecord);

    }
}
if (isset($_POST['back'])){
    header("Location:index.php");
}
function vratUzivatele($conPDO,$strana, $naStranu, $idUzivatele, $idAuta)
{
    $pocatekOd = ($strana-1)*$naStranu;
    $conPDO->setAttribute( PDO::ATTR_EMULATE_PREPARES, false );
    $stm= $conPDO->prepare("SELECT reservations.description, reservations.timedateFrom, reservations.timedateTo, 
users.name, cars.carName,destinations.destinationName, reservations.cars_idCars  FROM reservations
JOIN users ON users.id= reservations.users_idUser 
JOIN cars ON cars.id = reservations.cars_idCars 
JOIN destinations ON destinations.id = reservations.destinations_idDestinations
where reservations.cars_idCars = ?
 ORDER BY reservations.id DESC LIMIT ?,?");

    $stm->bindParam(1,$idAuta);
    $stm->bindParam(2,$pocatekOd);
    $stm->bindParam(3,$naStranu);

    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $usersResult= $stm->fetchAll();




    return $usersResult;
}
function vratPocetUzivatelu($conPDO)
{
    $stm= $conPDO->prepare("SELECT COUNT(*) FROM reservations");
    $stm->execute();
    $stm->setFetchMode(PDO::FETCH_NUM);
    $result = $stm->fetchAll();
    return $result[0][0];
}

if (isset($_GET['strana']))
    $strana = $_GET['strana'];
else
    $strana = 1;

$naStranu = 10;
$idUzivatele = $_SESSION['idUzivatele'];
$idAuta = $_GET['name'];
$reservationsResult = vratUzivatele($conPDO,$strana, $naStranu, $idUzivatele, $idAuta);
$stran = ceil(vratPocetUzivatelu($conPDO) / $naStranu);
?>




<div id="contenar">
    <header>    <h1 >Rezervační systém</h1>
        <div> <?php
            include "menu.php";
            ?></div>
    </header>


    <section>
        <div id="header">

            <h2>Vaše auta</h2>
            <table>
                <tr>
                    <th>Popis</th>
                    <th>Od: </th>
                    <th>Do: </th>
                    <th>Uzivatel</th>
                    <th>Auto</th>
                    <th>Destinace</th>


                </tr>

                <?php
                foreach ($reservationsResult as $reservationsData):

                    ?>
                    <tr>
                        <td><?php echo $reservationsData[0];?></td>
                        <td><?php echo $reservationsData[1];?></td>
                        <td><?php echo $reservationsData[2];?></td>
                        <td><?php echo $reservationsData[3];?></td>
                        <td><?php echo $reservationsData[4];?></td>
                        <td><?php echo $reservationsData[5];?></td>





                    </tr>
                <?php
                endforeach;
                ?>

            </table>
        </div>

    </section>




</div>
<?= paginace($strana, $stran, '?strana={strana}') ?>
</body>

</html>
