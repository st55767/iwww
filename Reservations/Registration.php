<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rezervační systém-Registrace</title>
    <link rel="stylesheet" type="text/css" href="./ReservationWithStyle.css">

</head>
<body>
<div id="contenair">
    <?php
    include('include/db_con.php');
    session_start();

    if (isset($_POST['Back'])) {
        header('Location:index.php');
        exit();
    }

?>

    <?php
    if(isset($_POST['Submit']))
    {
        $name = htmlspecialchars($_POST['name']);
        $surname = htmlspecialchars($_POST['surname']);
        $email = htmlspecialchars($_POST['email']);
        $password = htmlspecialchars($_POST['password']);

        if (empty($name) || empty($surname) || empty($email) ||empty($password))
        {
            $error = 'Musí být vyplněna všechna pole ';

        } else {

    try{
        $stm = $conPDO->prepare("SELECT email FROM users ");
        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $allemails = $stm->fetchAll();
        foreach ($allemails as $usersEmail):;
            if ($email == $usersEmail[0]) {
                throw new Exception("Zadaný email je již používán !");
            }
        endforeach;

        $query1 = "INSERT INTO users ( name, surname, email, password )VALUES(?,?,?,?)";
        $stm=$conPDO->prepare($query1);
        $stm->bindParam(1,$name);
        $stm->bindParam(2,$surname);
        $stm->bindParam(3,$email);
        $stm->bindParam(4,$password);
        $stm->execute();

        $usersQuery = "SELECT id FROM users where email = ? ";
        $stm= $conPDO->prepare($usersQuery);
        $stm->bindParam(1 ,$email);
        $stm->execute();
        $stm->setFetchMode(PDO::FETCH_NUM);
        $results=$stm->fetchAll();
        $Id =($results[0][0]);

        $stm= $conPDO->prepare("INSERT INTO users_has_roles (users_idUsers, roles_idRoles) VALUES (?,?)");
        $stm->bindParam(1,$Id);
        $idRole = 2;
        $stm->bindParam(2,$idRole);
        $stm->execute();

        header('Location:index.php');

    }catch (PDOException $ex){
      $error= "záznam nelze vložit";
    }catch (Exception $e){
        $error= $e->getMessage();
    }




        }

    }
    ?>


    <div >
        <header>
            <h1>Rezervační systém </h1>

        </header>
        <h3><u><i>Vytvoření účtu nového uživatele</i></u></h3>

            <form method="POST" name="signupform"  >
                <?php  if (isset($error)) {?>

                        <p> <?php echo $error; ?> </p>


                    <?php } ?>
                <table>


                <tr>
                    <td >Jmeno:</td>
                    <td><input name="name" type="text" id="name" size="40" />

                    </td>
                </tr>
                <tr>
                    <td >Prijmeni:</td>
                    <td><input name="surname" type="text" id="surname" size="40"  />

                    </td>
                </tr>

                <tr>
                    <td >E-mail:</td>
                    <td><input name="email" type="text" id="email" size="40"  />

                    </td>
                </tr>

                <tr>
                    <td>Heslo:</td>
                    <td><input name="password" type="password" id="password" size="40" />

                    </td>
                </tr>



                <tr>
                    <td>
                        <input type="submit" name="Submit" value="Vytvořit" />
                        <input type="submit" name="Back" value="Zpět" /></td>

                </tr>

        </table>
            </form>
    </div>

</div>


</body>
</html>

