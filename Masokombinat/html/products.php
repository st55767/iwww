<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rodinné řeznictví Zajíček</title>
    <link rel="stylesheet" href="../CSS/header.css">
    <link rel="stylesheet" href="../CSS/footerwithadress.css">
    <link rel="stylesheet" href="../CSS/stylesheet01.css">
    <link rel="stylesheet" href="../CSS/styleOfProducts.css">



</head>

<body>
<!-- container with background picture -->
<div class="backgroundpicture">
    <!--this div is for header menu with logo -->
    <div class="header" id="myHeader">
        <a href="index.php" class="logo">CompanyLogo</a>
        <div class="header-right">
            <a  href="index.php">Home</a>
            <a class="active" href="products.php">Produkty</a>
            <a href="about.php">O nás</a>
            <a href="contact.php">Kontakt</a>
        </div>
    </div>

</div>

<!-- container with main content, this is the container with changagle content, others should stay the same -->
<div class="maincontent">
<div id="wrap">
    <div id="columns" class="columns_4">
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>
        <figure>
            <img src="../Pictures/sunka.jpg">
            <figcaption>Šunka</figcaption>
            <span class="price">hodně peněz</span>
            <a class="button" href="#">Info</a>
        </figure>

  <!-- source: https://codepen.io/anhnt/pen/yfswK -->
    </div>

</div>







<!-- source of footer:https://demo.tutorialzine.com/2015/01/freebie-5-responsive-footer-templates/footer-distributed-with-address-and-phones.html -->
<footer class="footer-distributed">
    <div class="footer-left">
        <h3> "Company
            <span>logo</span>
        </h3>
        <p class="footer-links">
            <a href="#"> Domovská stránka</a>
            <a href="#"> Produkty</a>
            <a href="#"> O nás</a>
            <a href="#"> Contact</a>
        </p>
        <p class="footer-company-name"> Rodinné řeznictví Zajíček © 2020 </p>
    </div>
    <div class="footer-center">
        <div>
            <i class="fa-map-marker">
            </i>
            <p>
                <span> Hálkova 666, 396 01 Humpolec</span>
            </p>
        </div>
        <div>
            <i class="fa-phone"></i>
            <p> 728 449 601</p>
        </div>
        <div>
            <i class="fa-envelope"></i>
            <p>
                <a href="#"> email@email.cz</a>
            </p>
        </div>
    </div>
    <div class="footer-right">
        <p class="footer-company-about">
            <span> Krátce o vaší firmě</span>
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam id dolor. Duis viverra diam non justo.
            Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit."
        </p>
        <div class="footer-icons">
            <a href="#">
                <i class="fa-facebook">
                </i>
            </a>
        </div>
    </div>
</footer>
<script src="../JS/headerOnScroll.js"></script>

</body>
</html>
<?php

?>
