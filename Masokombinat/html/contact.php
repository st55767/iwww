<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rodinné řeznictví Zajíček</title>
    <link rel="stylesheet" href="../CSS/header.css">
    <link rel="stylesheet" href="../CSS/footerwithadress.css">
    <link rel="stylesheet" href="../CSS/stylesheet01.css">


</head>

<body>
<!-- container with background picture -->
<div class="backgroundpicture">
    <!--this div is for header menu with logo -->
    <div class="header" id="myHeader">
        <a href="index.php" class="logo">CompanyLogo</a>
        <div class="header-right">
            <a  href="index.php">Home</a>
            <a href="products.php">Produkty</a>
            <a href="about.php">O nás</a>
            <a class="active" href="contact.php">Kontakt</a>
        </div>
    </div>

</div>

<!-- container with main content, this is the container with changagle content, others should stay the same -->
<div class="maincontent">

    <div id="leftsideContact">
        <h1> Rodinné Řeznictví zajíček</h1>
        <ul>Hálkova 666</ul>
        <ul> 396 01 Humpolec</ul>
        <ul>Telefon: 728 449 601</ul>
        <h2>Otevírací doba</h2>
<!-- zarovnat -->
        <ul>pondělí	7:30–17</ul>
        <ul>úterý	7:30–17</ul>
        <ul>středa	7:30–17</ul>
        <ul>čtvrtek	7:30–17</ul>
        <ul>pátek	7:30–17</ul>
        <ul>sobota	7:30–11</ul>
        <ul>neděle	Zavřeno</ul>

    </div>
    <div id="rightsideContact">
        <h1>My First Google Map</h1>

        <iframe id="Gmap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2589.1536178058186!2d15.354155015698547!3d49.
        53825267935992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470cfdd73c88220d%3A0x3c0a393680d1562a!
        2sRodinn%C3%A9%20Reznictvi%20Zajicek!5e0!3m2!1scs!2scz!4v1601591562462!5m2!1scs!2scz"
                width="800" height="450" frameborder="0" style="border:0;" allowfullscreen=""
                aria-hidden="false" tabindex="0"></iframe>
    </div>
</div>

<!-- source of footer:https://demo.tutorialzine.com/2015/01/freebie-5-responsive-footer-templates/footer-distributed-with-address-and-phones.html -->
<footer class="footer-distributed">
    <div class="footer-left">
        <h3> "Company
            <span>logo</span>
        </h3>
        <p class="footer-links">
            <a href="#"> Domovská stránka</a>
            <a href="#"> Produkty</a>
            <a href="#"> O nás</a>
            <a href="#"> Contact</a>
        </p>
        <p class="footer-company-name"> Rodinné řeznictví Zajíček © 2020 </p>
    </div>
    <div class="footer-center">
        <div>
            <i class="fa-map-marker">
            </i>
            <p>
                <span> Hálkova 666, 396 01 Humpolec</span>
            </p>
        </div>
        <div>
            <i class="fa-phone"></i>
            <p> 728 449 601</p>
        </div>
        <div>
            <i class="fa-envelope"></i>
            <p>
                <a href="#"> email@email.cz</a>
            </p>
        </div>
    </div>
    <div class="footer-right">
        <p class="footer-company-about">
            <span> Krátce o vaší firmě</span>
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam id dolor. Duis viverra diam non justo.
            Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit."
        </p>
        <div class="footer-icons">
            <a href="#">
                <i class="fa-facebook">
                </i>
            </a>
        </div>
    </div>
</footer>
<script src="../JS/headerOnScroll.js"></script>

</body>
</html>
<?php

?>
