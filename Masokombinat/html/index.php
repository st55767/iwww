<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Rodinné řeznictví Zajíček</title>
    <link rel="stylesheet" href="../CSS/header.css">
    <link rel="stylesheet" href="../CSS/footerwithadress.css">
    <link rel="stylesheet" href="../CSS/stylesheet01.css">


</head>

<body>
<!-- container with background picture -->
<div class="backgroundpicture">
    <!--this div is for header menu with logo -->
    <div class="header" id="myHeader">
        <a href="index.php" class="logo">CompanyLogo</a>
        <div class="header-right">
            <a class="active" href="index.php">Home</a>
            <a href="products.php">Produkty</a>
            <a href="about.php">O nás</a>
            <a href="contact.php">Kontakt</a>
        </div>
    </div>

</div>

<!-- container with main content, this is the container with changagle content, others should stay the same -->
<div class="maincontent">
    <div id="leftside">
        <h2>Aktuality a upozornění</h2>
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nunc dapibus tortor vel mi dapibus sollicitudin.
        In dapibus augue non sapien. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.
        Quisque tincidunt scelerisque libero. Proin in tellus sit amet nibh dignissim sagittis. Aliquam erat volutpat.
        Nulla quis diam. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Sed convallis magna eu sem.
        Aenean fermentum risus id tortor. Aliquam erat volutpat.

        Maecenas aliquet accumsan leo. Et harum quidem rerum facilis est et expedita distinctio.
        Nam sed tellus id magna elementum tincidunt. Phasellus faucibus molestie nisl. Nulla est.
        Integer malesuada. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae
        consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Nullam eget nisl.
        Suspendisse sagittis ultrices augue. Phasellus rhoncus. Cum sociis natoque penatibus et magnis dis parturient
        montes, nascetur ridiculus mus. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Nullam dapibus
        fermentum ipsum. Nunc dapibus tortor vel mi dapibus sollicitudin. Integer rutrum, orci vestibulum ullamcorper
        ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Maecenas lorem. Duis
    </div>
    <div id="rightside">
 <h2>Otevírací doba</h2>

            <ul>pondělí	7:30–17</ul>
            <ul>úterý	7:30–17</ul>
            <ul>středa	7:30–17</ul>
            <ul>čtvrtek	7:30–17</ul>
            <ul>pátek	7:30–17</ul>
            <ul>sobota	7:30–11</ul>
            <ul>neděle	Zavřeno</ul>

    </div>
    <div id="under1">
        <h2>Covid opatření</h2>
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nunc dapibus tortor vel mi dapibus sollicitudin.
        In dapibus augue non sapien. Maecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.
        Quisque tincidunt scelerisque libero. Proin in tellus sit amet nibh dignissim sagittis. Aliquam erat volutpat.
        Nulla quis diam. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est. Sed convallis magna eu sem.
        Aenean fermentum risus id tortor. Aliquam erat volutpat.

        Maecenas aliquet accumsan leo. Et harum quidem rerum facilis est et expedita distinctio.
        Nam sed tellus id magna elementum tincidunt. Phasellus faucibus molestie nisl. Nulla est.
        Integer malesuada. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae
        consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Nullam eget nisl.
        Suspendisse sagittis ultrices augue. Phasellus rhoncus. Cum sociis natoque penatibus et magnis dis parturient
        montes, nascetur ridiculus mus. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Nullam dapibus
        fermentum ipsum. Nunc dapibus tortor vel mi dapibus sollicitudin. Integer rutrum, orci vestibulum ullamcorper
        ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Maecenas lorem. Duis
    </div>
</div>

<!-- source of footer:https://demo.tutorialzine.com/2015/01/freebie-5-responsive-footer-templates/footer-distributed-with-address-and-phones.html -->
<footer class="footer-distributed">
    <div class="footer-left">
    <h3> "Company
    <span>logo</span>
    </h3>
        <p class="footer-links">
            <a href="#"> Domovská stránka</a>
            <a href="#"> Produkty</a>
            <a href="#"> O nás</a>
            <a href="#"> Contact</a>
        </p>
        <p class="footer-company-name"> Rodinné řeznictví Zajíček © 2020 </p>
    </div>
    <div class="footer-center">
        <div>
            <i class="fa-map-marker">
            </i>
            <p>
                <span> Hálkova 666, 396 01 Humpolec</span>
            </p>
        </div>
        <div>
            <i class="fa-phone"></i>
            <p> 728 449 601</p>
        </div>
        <div>
            <i class="fa-envelope"></i>
            <p>
                <a href="#"> email@email.cz</a>
            </p>
        </div>
    </div>
    <div class="footer-right">
    <p class="footer-company-about">
        <span> Krátce o vaší firmě</span>
        "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam id dolor. Duis viverra diam non justo.
        Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit."
    </p>
        <div class="footer-icons">
            <a href="#">
                <i class="fa-facebook">
                </i>
            </a>
        </div>
    </div>
</footer>
<script src="../JS/headerOnScroll.js"></script>

</body>
</html>
<?php

?>
